// Copyright 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
//
// This file is part of Hop.
//
// Hop is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Hop is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Hop.  If not, see <http://www.gnu.org/licenses/>.
//
package hop;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

/**
 */
public class Subscription {
    public ServerApi _api;
    public String _source;
    public Object _filter;
    public String _consumerName;
    public HalfQueue _consumer;
    public Object _subscriptionToken;

    public Subscription(ServerApi api, String source, Object filter) throws InterruptedException, IOException {
        _api = api;
        _source = source;
        _filter = filter;
        _consumerName = UUID.randomUUID().toString();
        _consumer = new HalfQueue();
        _api._container.bind(_consumerName, _consumer);
        _subscriptionToken = _api.subscribe(source, filter, _consumerName);
    }

    public BlockingQueue<Object> getQueue() {
        return _consumer.getQueue();
    }

    public void unsubscribe() throws IOException {
        _api.unsubscribe(_source, _subscriptionToken);
    }
}
