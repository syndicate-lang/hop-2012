// Copyright 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
//
// This file is part of Hop.
//
// Hop is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Hop is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Hop.  If not, see <http://www.gnu.org/licenses/>.
//
package hop;

import java.io.IOException;
import java.io.OutputStream;

/**
 */
public class SexpDisplayHint extends SexpBytes {
    public byte[] _hint;

    public SexpDisplayHint(byte[] hint, byte[] body) {
        super(body);
        _hint = hint;
    }

    public byte[] getHint() {
        return _hint;
    }

    public void writeTo(OutputStream stream) throws IOException {
        stream.write('[');
        SexpWriter.writeSimpleString(stream, _hint);
        stream.write(']');
        super.writeTo(stream);
    }
}
