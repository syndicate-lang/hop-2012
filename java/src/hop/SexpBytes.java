// Copyright 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
//
// This file is part of Hop.
//
// Hop is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Hop is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Hop.  If not, see <http://www.gnu.org/licenses/>.
//
package hop;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

/**
 */
public class SexpBytes {
    public byte[] _bytes;

    public SexpBytes(byte[] bytes) {
        _bytes = bytes;
    }

    public byte[] getData() {
        return _bytes;
    }

    public String getDataString() {
        return new String(getData());
    }

    public void writeTo(OutputStream stream) throws IOException {
        SexpWriter.writeSimpleString(stream, _bytes);
    }

    public String toString() {
        return SexpWriter.writeString(this);
    }

    public boolean equals(Object other) {
        return (other instanceof SexpBytes) &&
            Arrays.equals(_bytes, ((SexpBytes) other).getData());
    }

    public int hashCode() {
        return Arrays.hashCode(_bytes);
    }
}
