// Copyright 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
//
// This file is part of Hop.
//
// Hop is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Hop is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Hop.  If not, see <http://www.gnu.org/licenses/>.
//
package hop;

import java.util.ArrayList;

/**
 */
public class SexpList extends ArrayList<Object> {
    public SexpBytes getBytes(int index) throws SexpSyntaxError {
        Object x = get(index);
        if (x != null && !(x instanceof SexpBytes)) {
            throw new SexpSyntaxError("Unexpected non-bytes");
        }
        return (SexpBytes) get(index);
    }

    public SexpList getList(int index) throws SexpSyntaxError {
        Object x = get(index);
        if (x != null && !(x instanceof SexpList)) {
            throw new SexpSyntaxError("Unexpected non-list");
        }
        return (SexpList) get(index);
    }

    public static SexpList empty() {
        return new SexpList();
    }

    public static SexpList with(Object x) {
        return empty().and(x);
    }

    public SexpList and(Object x) {
        this.add(x);
        return this;
    }
}
