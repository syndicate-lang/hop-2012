// Copyright 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
//
// This file is part of Hop.
//
// Hop is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Hop is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Hop.  If not, see <http://www.gnu.org/licenses/>.
//
package hop;

/**
 */
public class SexpMessage {
    public static SexpBytes _post = new SexpBytes("post".getBytes());
    public static SexpBytes _subscribe = new SexpBytes("subscribe".getBytes());
    public static SexpBytes _unsubscribe = new SexpBytes("unsubscribe".getBytes());
    public static SexpBytes _subscribe_ok = new SexpBytes("subscribe-ok".getBytes());
    public static SexpBytes _create = new SexpBytes("create".getBytes());
    public static SexpBytes _create_ok = new SexpBytes("create-ok".getBytes());
    public static SexpBytes _create_failed = new SexpBytes("create-failed".getBytes());

    public static SexpList post(Object name, Object message, Object token) {
        SexpList m = new SexpList();
        m.add(_post);
        m.add(name);
        m.add(message);
        m.add(token);
        return m;
    }

    public static SexpList subscribe(Object filter, String sink, Object name, String replySink, Object replyName) {
        SexpList m = new SexpList();
        m.add(_subscribe);
        m.add(filter);
        m.add(sink);
        m.add(name);
        m.add(replySink);
        m.add(replyName);
        return m;
    }

    public static SexpList subscribe_ok(Object token) {
        SexpList m = new SexpList();
        m.add(_subscribe_ok);
        m.add(token);
        return m;
    }

    public static SexpList unsubscribe(Object token) {
        SexpList m = new SexpList();
        m.add(_unsubscribe);
        m.add(token);
        return m;
    }

    public static SexpList create(String nodeClassName, Object arg, String replySink, Object replyName) {
        SexpList m = new SexpList();
        m.add(_create);
        m.add(nodeClassName);
        m.add(arg);
        m.add(replySink);
        m.add(replyName);
        return m;
    }
}
