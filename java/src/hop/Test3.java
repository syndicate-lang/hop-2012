// Copyright 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
//
// This file is part of Hop.
//
// Hop is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Hop is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Hop.  If not, see <http://www.gnu.org/licenses/>.
//
package hop;

import java.io.IOException;

/**
 */
public class Test3 {
    public static void main(String[] args) {
        try {
            run(args[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void run(String hostname) throws IOException, InterruptedException {
        NodeContainer nc = new NodeContainer();

        System.out.println("Hostname: " + hostname);

        System.out.println("Container: " + nc.getName());

        Relay r = new Relay(nc, hostname);
        ServerApi api = new ServerApi(nc, r.getRemoteName());

        api.createQueue("q1");
        long startTime = System.currentTimeMillis();
        int count = 0;
        for (int i = 0; i < 10000000; i++) {
            api.post("q1", null, Integer.toString(i), null);
            count++;
            if ((count % 100000) == 0) {
                api.flush();
                long now = System.currentTimeMillis();
                double delta = (now - startTime) / 1000.0;
                System.out.println("Sent "+count+" messages in "+delta+" seconds, rate = " + (count / delta) + " Hz");
            }
        }
    }
}
