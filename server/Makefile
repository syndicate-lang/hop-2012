APP=hop_server
TEMPLATES=$(wildcard web/bootstrap/templates/*.xml)
HTML=$(subst web/bootstrap/templates/,web/,$(subst .xml,.html,$(TEMPLATES)))

# Static builds. So far I've only seen this work on Linux. OS X complains about missing -lcrt0.o.
# OCAMLBUILD=ocamlbuild -classic-display -use-ocamlfind -X scratch -lflag -cclib -lflag -static
OCAMLBUILD=ocamlbuild -tag thread -classic-display -use-ocamlfind -X scratch

all: \
	message.ml amqp_spec.ml \
	$(APP).native \
	webpages

webpages: $(HTML) web/bootstrap/css/bootstrap.css

web/bootstrap/css/bootstrap.css: web/bootstrap/less/*.less
	recess --compile web/bootstrap/less/bootstrap.less > $@

web/%.html: web/bootstrap/templates/%.xml web/bootstrap/template.xsl web/bootstrap/nav.xml
	xsltproc web/bootstrap/template.xsl $< > $@

message.ml: ../protocol/messages.json codegen.py
	python3 codegen.py > $@

amqp_spec.ml: amqp0-9-1.stripped.xml amqp_codegen.py
	python3 amqp_codegen.py > $@

webclean:
	rm -f $(HTML)

clean: webclean
	ocamlbuild -clean
	rm -f message.ml
	rm -f amqp_spec.ml

veryclean: clean
	rm -f web/bootstrap/css/bootstrap.css

$(APP).native: $(wildcard *.ml) lwt_installed
	$(OCAMLBUILD) $@

$(APP).p.native: $(wildcard *.ml) lwt_installed
	$(OCAMLBUILD) $@

lwt_installed:
	opam install camlp4 lwt lwt_camlp4

run: all
	./$(APP).native
