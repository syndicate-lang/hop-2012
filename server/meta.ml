(* Copyright 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>. *)

(* This file is part of Hop. *)

(* Hop is free software: you can redistribute it and/or modify it *)
(* under the terms of the GNU General Public License as published by the *)
(* Free Software Foundation, either version 3 of the License, or (at your *)
(* option) any later version. *)

(* Hop is distributed in the hope that it will be useful, but *)
(* WITHOUT ANY WARRANTY; without even the implied warranty of *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU *)
(* General Public License for more details. *)

(* You should have received a copy of the GNU General Public License *)
(* along with Hop.  If not, see <http://www.gnu.org/licenses/>. *)

open Sexp

let n_meta = Node.name_of_bytes (Bytes.of_string "meta")

let announce_subscription source filter sink name on_off =
  Node.post_ignore n_meta (Str source.Node.label)
    (if on_off
    then Message.subscribed (Str source.Node.label, filter, Str sink, name)
    else Message.unsubscribed (Str source.Node.label, filter, Str sink, name))
    emptystr

let init () =
  Node.send_ignore' (Bytes.of_string "factory")
    (Message.create (litstr "direct", Arr [litstr "meta"], emptystr, emptystr))
