(* Copyright 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>. *)

(* This file is part of Hop. *)

(* Hop is free software: you can redistribute it and/or modify it *)
(* under the terms of the GNU General Public License as published by the *)
(* Free Software Foundation, either version 3 of the License, or (at your *)
(* option) any later version. *)

(* Hop is distributed in the hope that it will be useful, but *)
(* WITHOUT ANY WARRANTY; without even the implied warranty of *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU *)
(* General Public License for more details. *)

(* You should have received a copy of the GNU General Public License *)
(* along with Hop.  If not, see <http://www.gnu.org/licenses/>. *)

open Lwt_unix

let rec accept_loop sock connection_start_fn =
  lwt (s, peername) = accept sock in
  setsockopt s Unix.TCP_NODELAY true;
  ignore (connection_start_fn (s, peername));
  accept_loop sock connection_start_fn

let start_net protocol_name port_number connection_start_fn =
  let sock = socket Unix.PF_INET Unix.SOCK_STREAM 0 in
  setsockopt sock Unix.SO_REUSEADDR true;
  lwt () = bind sock (Unix.ADDR_INET (Unix.inet_addr_any, port_number)) in
  listen sock 5;
  Server_control.milestone (Bytes.of_string (protocol_name ^ " ready"));
  ignore (Log.info "Accepting connections" [Sexp.str protocol_name; Sexp.str (string_of_int port_number)]);
  accept_loop sock connection_start_fn
