(* Copyright 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>. *)

(* This file is part of Hop. *)

(* Hop is free software: you can redistribute it and/or modify it *)
(* under the terms of the GNU General Public License as published by the *)
(* Free Software Foundation, either version 3 of the License, or (at your *)
(* option) any later version. *)

(* Hop is distributed in the hope that it will be useful, but *)
(* WITHOUT ANY WARRANTY; without even the implied warranty of *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU *)
(* General Public License for more details. *)

(* You should have received a copy of the GNU General Public License *)
(* along with Hop.  If not, see <http://www.gnu.org/licenses/>. *)

let bytes_of_revlist acc len =
  let buf = Bytes.make len ' ' in
  let rec fill cs i =
    match cs with
    | [] -> ()
    | c :: cs' -> (Bytes.set buf i c; fill cs' (i - 1))
  in
  fill acc (len - 1);
  buf

let string_of_revlist acc len =
  Bytes.to_string (bytes_of_revlist acc len)
