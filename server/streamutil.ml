(* Copyright 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>. *)

(* This file is part of Hop. *)

(* Hop is free software: you can redistribute it and/or modify it *)
(* under the terms of the GNU General Public License as published by the *)
(* Free Software Foundation, either version 3 of the License, or (at your *)
(* option) any later version. *)

(* Hop is distributed in the hope that it will be useful, but *)
(* WITHOUT ANY WARRANTY; without even the implied warranty of *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU *)
(* General Public License for more details. *)

(* You should have received a copy of the GNU General Public License *)
(* along with Hop.  If not, see <http://www.gnu.org/licenses/>. *)

open Lwt

let stream_to_bytes s =
  lwt pieces = Lwt_stream.to_list s in
  return (Bytes.concat Bytes.empty pieces)

let stream_generator f =
  let mbox = Lwt_mvar.create_empty () in
  let yield v = Lwt_mvar.put mbox (Some v) in
  ignore (lwt () = f yield in
	  Lwt_mvar.put mbox None);
  Lwt_stream.from (fun () -> Lwt_mvar.take mbox)

let stream_encode s = Lwt_stream.map Bytes.of_string s
