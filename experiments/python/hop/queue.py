## Copyright 2010, 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
##
## This file is part of Hop.
##
## Hop is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Hop is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
## or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
## License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Hop.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import with_statement

import logging
import threading
import Queue as builtin_Queue

import namespace
import factory
import dispatch
import subscription

Q = builtin_Queue.Queue

class Queue(dispatch.HopNode):
    def __init__(self, arg):
        self.name = arg[0]
        self.backlog = Q()
        self.waiters = Q()
        self.thread = threading.Thread(target = self.queue_main)
        self.thread.start()
        if not namespace.bind(self.name, self):
            raise Exception("duplicate name")

    def node_info(self):
        return []

    def hop_subscribe(self, filter, sink, name, replysink, replyname):
        sub = subscription.Subscription(filter, sink, name)
        self.waiters.put(sub)
        namespace.post(replysink, replyname, ['subscribe-ok', sub.uuid], '')

    def hop_post(self, name, body, token):
        self.backlog.put(body)

    def queue_main(self):
        while True:
            body = self.backlog.get()
            while True:
                waiter = self.waiters.get()
                if not waiter.deliver(body):
                    continue
                self.waiters.put(waiter)
                break

factory.default_factory.register('queue', Queue)
