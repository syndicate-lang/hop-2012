## Copyright 2010, 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
##
## This file is part of Hop.
##
## Hop is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Hop is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
## or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
## License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Hop.  If not, see <http://www.gnu.org/licenses/>.

import uuid
import logging

class Namespace:
    def __init__(self):
        self.nodename = str(uuid.uuid4())
        self.bindings = {}

    def bind(self, name, node):
        if name in self.bindings:
            return False
        self.bindings[name] = node
        return True

    def unbind(self, name):
        del self.bindings[name]

    def send(self, name, msg):
        ## logging.debug('SENDING TO %s: %r' % (name, msg))
        if name:
            if name in self.bindings:
                self.bindings[name].handle_hop(msg)
                return True
            else:
                logging.warning("Send to unbound name: %s" % (name,))
                return False
        else:
            return False

    def post(self, sink, name, body, token):
        return self.send(sink, ['post', name, body, token])

default_namespace = Namespace()
bind = default_namespace.bind
unbind = default_namespace.unbind
send = default_namespace.send
post = default_namespace.post
