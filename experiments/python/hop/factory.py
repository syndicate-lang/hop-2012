## Copyright 2010, 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
##
## This file is part of Hop.
##
## Hop is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Hop is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
## or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
## License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Hop.  If not, see <http://www.gnu.org/licenses/>.

import dispatch
import namespace

class Factory(dispatch.HopNode):
    def __init__(self):
        self.classes = {}

    def register(self, classname, classval):
        self.classes[classname] = classval

    def hop_create(self, classname, arg, replysink, replyname):
        if classname not in self.classes:
            namespace.post(replysink, replyname,
                           ['create-failed', ['factory', 'class-not-found']], '')
            return

        try:
            node = self.classes[classname](arg)
            namespace.post(replysink, replyname, ['create-ok', node.node_info()], '')
        except Exception, e:
            import traceback
            traceback.print_exc()
            namespace.post(replysink, replyname, ['create-failed', ['constructor', str(e)]], '')

default_factory = Factory()
namespace.bind('factory', default_factory)
