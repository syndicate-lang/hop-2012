/* Copyright 2010, 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
 *
 * This file is part of Hop.
 *
 * Hop is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hop is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hop.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef cmsg_dataq_h
#define cmsg_dataq_h

typedef struct queue_t_ {
  size_t link_offset;
  int count;
  void *head;
  void *tail;
} queue_t;

#define EMPTY_QUEUE(element_t, link_field_name) \
  ((queue_t) { offsetof(element_t, link_field_name), 0, NULL, NULL })

extern void enqueue(queue_t *q, void *x);
extern void *dequeue(queue_t *q);

extern void queue_append(queue_t *q1, queue_t *q2);

#endif
