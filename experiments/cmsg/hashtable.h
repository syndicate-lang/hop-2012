/* Copyright 2010, 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
 *
 * This file is part of Hop.
 *
 * Hop is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hop is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hop.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef cmsg_hashtable_h
#define cmsg_hashtable_h

typedef struct hashtable_entry_t_ {
  struct hashtable_entry_t_ *next;
  cmsg_bytes_t key;
  void *value;
} hashtable_entry_t;

typedef struct hashtable_t_ {
  size_t bucket_count;
  size_t entry_count;
  hashtable_entry_t **buckets;
  void *(*dup_value)(void *);
  void (*free_value)(void *);
} hashtable_t;

typedef void (*hashtable_iterator_t)(void *context, cmsg_bytes_t key, void *value);

extern uint32_t hash_bytes(cmsg_bytes_t bytes);

extern void init_hashtable(hashtable_t *table,
			   size_t initial_bucket_count,
			   void *(*dup_value)(void *),
			   void (*free_value)(void *));
extern void destroy_hashtable(hashtable_t *table);

extern int hashtable_contains(hashtable_t *table, cmsg_bytes_t key);
extern int hashtable_get(hashtable_t *table, cmsg_bytes_t key, void **valueptr);
extern int hashtable_put(hashtable_t *table, cmsg_bytes_t key, void *value);
extern int hashtable_erase(hashtable_t *table, cmsg_bytes_t key);
extern void hashtable_foreach(hashtable_t *table,
			      hashtable_iterator_t iterator,
			      void *context);

#endif
