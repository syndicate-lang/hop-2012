/* Copyright 2010, 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
 *
 * This file is part of Hop.
 *
 * Hop is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hop is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hop.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef cmsg_node_h
#define cmsg_node_h

typedef struct node_t_ {
  refcount_t refcount;
  struct node_class_t_ *node_class;
  hashtable_t names;
  void *extension; /* Each node class puts something different here in its instances */
} node_t;

typedef sexp_t *(*node_extension_fn_t)(node_t *n, sexp_t *args);
typedef void (*node_destructor_fn_t)(node_t *n);
typedef void (*node_message_handler_fn_t)(node_t *n, sexp_t *m);

typedef struct node_class_t_ {
  char const *name;
  node_extension_fn_t extend;
  node_destructor_fn_t destroy;
  node_message_handler_fn_t handle_message;
} node_class_t;

extern void init_node(cmsg_bytes_t container_name);

extern cmsg_bytes_t local_container_name(void);

extern void basic_node_destroy(node_t *n);

extern void register_node_class(node_class_t *nc);
extern node_class_t *lookup_node_class(cmsg_bytes_t name);

extern node_t *new_node(node_class_t *nc, sexp_t *args, sexp_t **error_out);
extern void node_destructor(node_t *n);

extern node_t *lookup_node(cmsg_bytes_t name);
extern int bind_node(cmsg_bytes_t name, node_t *n);
extern int unbind_node(cmsg_bytes_t name);
extern void unbind_all_names_for_node(node_t *n);

extern int post_node(cmsg_bytes_t node, cmsg_bytes_t name, sexp_t *body, sexp_t *token);
extern int send_node(cmsg_bytes_t node, sexp_t *message);
extern int send_node_release(cmsg_bytes_t node, sexp_t *message);

#endif
