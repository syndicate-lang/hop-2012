/* Copyright 2010, 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
 *
 * This file is part of Hop.
 *
 * Hop is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hop is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hop.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

#include "dataq.h"

#define QLINK(q,x)	(*((void **)(((char *) x) + (q)->link_offset)))

void enqueue(queue_t *q, void *x) {
  QLINK(q, x) = NULL;
  if (q->head == NULL) {
    q->head = x;
  } else {
    QLINK(q, q->tail) = x;
  }
  q->tail = x;
  q->count++;
}

void *dequeue(queue_t *q) {
  if (q->head == NULL) {
    return NULL;
  } else {
    void *x = q->head;
    q->head = QLINK(q, x);
    QLINK(q, x) = NULL;
    if (q->head == NULL) {
      q->tail = NULL;
    }
    q->count--;
    return x;
  }
}

void queue_append(queue_t *q1, queue_t *q2) {
  assert(q1->link_offset == q2->link_offset);

  if (q2->head != NULL) {
    if (q1->head != NULL) {
      QLINK(q1, q1->tail) = q2->head;
    } else {
      q1->head = q2->head;
    }
    q1->tail = q2->tail;
    q2->head = NULL;
    q2->tail = NULL;
  }
}
