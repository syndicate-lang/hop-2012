/* Copyright 2010, 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
 *
 * This file is part of Hop.
 *
 * Hop is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hop is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hop.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef cmsg_sexpio_h
#define cmsg_sexpio_h

#define SEXP_ERROR_OVERFLOW	0x8000
#define SEXP_ERROR_SYNTAX	0x8001

extern sexp_t *sexp_read_atom(IOHandle *h);
extern int sexp_read(IOHandle *h, sexp_t **result_ptr);
extern unsigned short sexp_write(IOHandle *h, sexp_t *x);
extern unsigned short sexp_writeln(IOHandle *h, sexp_t *x);

#endif
