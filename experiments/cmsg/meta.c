/* Copyright 2010, 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
 *
 * This file is part of Hop.
 *
 * Hop is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hop is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hop.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <stdint.h>
#include <stddef.h>

#include <assert.h>

#include "cmsg_private.h"
#include "ref.h"
#include "sexp.h"
#include "hashtable.h"
#include "node.h"
#include "meta.h"
#include "messages.h"

void init_meta(void) {
  sexp_t *args;
  args = INCREF(sexp_cons(sexp_cstring("meta"), NULL));
  new_node(lookup_node_class(cmsg_cstring_bytes("direct")), args, NULL);
  DECREF(args, sexp_destructor);
}

void announce_subscription(sexp_t *source,
			   sexp_t *filter,
			   sexp_t *sink,
			   sexp_t *name,
			   int onoff)
{
  post_node(cmsg_cstring_bytes("meta"),
	    sexp_data(source),
	    onoff
	      ? message_subscribed(source, filter, sink, name)
	      : message_unsubscribed(source, filter, sink, name),
	    NULL);
}
