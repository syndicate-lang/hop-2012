/* Copyright 2010, 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
 *
 * This file is part of Hop.
 *
 * Hop is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hop is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hop.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef cmsg_private_h
#define cmsg_private_h

typedef struct cmsg_bytes_t {
  size_t len;
  unsigned char *bytes;
} cmsg_bytes_t;

#define CMSG_BYTES(length, bytes_ptr) ((cmsg_bytes_t) { \
      .len = (length),					\
      .bytes = (unsigned char *) (bytes_ptr)		\
    })
#define EMPTY_BYTES CMSG_BYTES(0, NULL)

static inline cmsg_bytes_t cmsg_cstring_bytes(char const *cstr) {
  cmsg_bytes_t result;
  result.len = strlen(cstr);
  result.bytes = (void *) cstr;
  return result;
}

#define CMSG_UUID_BUF_SIZE 36
extern int gen_uuid(unsigned char *uuid_buf); /* must be exactly CMSG_UUID_BUF_SIZE bytes long */

extern cmsg_bytes_t cmsg_bytes_malloc_dup(cmsg_bytes_t src);
extern cmsg_bytes_t cmsg_bytes_malloc(size_t amount);
extern void cmsg_bytes_free(cmsg_bytes_t bytes);
extern int cmsg_bytes_cmp(cmsg_bytes_t a, cmsg_bytes_t b);

#define ICHECK(result, message) do { if ((result) == -1) { perror(message); exit(2); } } while (0)
#define BCHECK(result, message) do { if ((result) == 0) { perror(message); exit(2); } } while (0)
#define PCHECK(result, message) do { if ((result) == NULL) { perror(message); exit(2); } } while (0)

extern __attribute__((noreturn)) void die(char const *format, ...);
extern void warn(char const *format, ...);
extern void info(char const *format, ...);

#endif
