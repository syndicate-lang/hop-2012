/* Copyright 2010, 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
 *
 * This file is part of Hop.
 *
 * Hop is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hop is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hop.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef cmsg_subscription_h
#define cmsg_subscription_h

typedef struct subscription_t_ {
  sexp_t *uuid;
  sexp_t *filter;
  sexp_t *sink;
  sexp_t *name;
  struct subscription_t_ *link;
} subscription_t;

extern void free_subscription(subscription_t *sub);
extern void free_subscription_chain(subscription_t *chain);

extern int send_to_subscription(sexp_t *source,
				hashtable_t *subscriptions,
				subscription_t *sub,
				sexp_t *body);
extern subscription_t *send_to_subscription_chain(sexp_t *source,
						  hashtable_t *subscriptions,
						  subscription_t *chain,
						  sexp_t *body);

extern subscription_t *handle_subscribe_message(sexp_t *source,
						hashtable_t *subscriptions,
						parsed_message_t *p);

extern void handle_unsubscribe_message(sexp_t *source,
				       hashtable_t *subscriptions,
				       parsed_message_t *p);

#endif
