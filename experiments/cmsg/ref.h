/* Copyright 2010, 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
 *
 * This file is part of Hop.
 *
 * Hop is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hop is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hop.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef cmsg_ref_h
#define cmsg_ref_h

typedef struct refcount_t_ {
  unsigned int count;
} refcount_t;

#define ZERO_REFCOUNT() ((refcount_t) { .count = 0 })

#define INCREF(x) ({				\
      typeof(x) __x = (x);			\
      if (__x != NULL) {			\
	__x->refcount.count++;			\
      }						\
      __x;					\
    })

#define UNGRAB(x) ({				\
      typeof(x) __x = (x);			\
      if (__x != NULL) {			\
	assert(__x->refcount.count);		\
	__x->refcount.count--;			\
      }						\
      __x;					\
    })

#define DECREF(x, dtor) ({			\
    typeof(x) __x = (x);			\
    if (__x != NULL) {				\
      assert(__x->refcount.count);		\
      (__x->refcount.count)--;			\
      if (__x->refcount.count == 0) {		\
	(dtor)(__x);				\
      }						\
    }						\
    (typeof(__x)) 0;				\
  })

#endif
