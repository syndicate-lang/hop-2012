%% Copyright 2010, 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
%%
%% This file is part of Hop.
%%
%% Hop is free software: you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% Hop is distributed in the hope that it will be useful, but WITHOUT
%% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
%% or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
%% License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Hop.  If not, see <http://www.gnu.org/licenses/>.

-module(hop).

-export([name/0, register_idempotent/3, class_of/1, send/2, post/4]).

name() ->
    list_to_binary(atom_to_list(node())).

register_idempotent(Name, Pid, ClassModule) ->
    case global:register_name(Name, Pid) of
        yes ->
            ok;
        no ->
            case class_of(Name) of
                undefined ->
                    register_idempotent(Name, Pid, ClassModule);
                ClassModule ->
                    ok;
                _ ->
                    {error, <<"class-mismatch">>}
            end
    end.

class_of(Name) ->
    case global:whereis_name(Name) of
        undefined ->
            undefined;
        Pid ->
            gen_server:call(Pid, hop_class_module)
    end.

send(<<>>, _Body) ->
    ok;
send(Name, Body) ->
    case global:whereis_name(Name) of
        undefined ->
            error_logger:warning_report({?MODULE, send, undefined_name, Name}),
            false;
        Pid ->
            Pid ! {hop, Body},
            true
    end.

post(Sink, Name, Body, Token) ->
    send(Sink, [<<"post">>, Name, Body, Token]).
