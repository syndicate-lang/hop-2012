%% Copyright 2010, 2011, 2012 Tony Garnock-Jones <tonygarnockjones@gmail.com>.
%%
%% This file is part of Hop.
%%
%% Hop is free software: you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% Hop is distributed in the hope that it will be useful, but WITHOUT
%% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
%% or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
%% License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with Hop.  If not, see <http://www.gnu.org/licenses/>.

-module(hop_demo).

-export([start/1]).

start([Port]) ->
    hop_factory:start_link([]),
    ok = hop_factory:register_class(<<"queue">>, hop_queue),
    hop_server:start_link(hop_relay, "0.0.0.0", list_to_integer(Port),
                          [{reuseaddr, true}, {active, false}],
                          []).
