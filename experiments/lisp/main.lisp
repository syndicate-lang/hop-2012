(ql:quickload "flexi-streams")
;(ql:quickload "babel")
(ql:quickload "usocket")
(ql:quickload "cl-match")

(ql:quickload "gbbopen")
(require :portable-threads)

(load "packages.lisp")
(load "sexp.lisp")
(load "network.lisp")

(in-package :cl-user)

;; (defun handle-connection (stream)
;;   (spki-sexp:write-sexp (spki-sexp:read-sexp stream) stream))

;; (defun start-server (port)
;;   (usocket:socket-server "localhost" port 'handle-connection '()
;; 			 :in-new-thread t
;; 			 :multi-threading t
;; 			 :reuse-address t
;; 			 :element-type '(unsigned-byte 8)))

;; (start-server 5671)

(smsg-network:serve-on-port 5671)

;; (let ((server-socket (socket-listen "localhost" 5671
;; 				    :reuse-address t
;; 				    :element-type unsigned-integer)))
;;   (loop for conn = (socket-accept server-socket)
;;        do (handle-connection conn)))
