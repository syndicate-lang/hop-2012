#!/bin/sh

RELEASE_RESOURCES="$1"
SERVER_BINARY="$2"

cd "$RELEASE_RESOURCES"

LIBEV_PATH=$(otool -L "$SERVER_BINARY" | grep 'libev.*dylib' | awk '{print $1}')

if [ ! -f "$LIBEV_PATH" ];
then
    echo "Not rewriting LIBEV_PATH $LIBEV_PATH"
    exit 0
fi

LIBEV_LOCAL=$(basename "$LIBEV_PATH")
cp $LIBEV_PATH $LIBEV_LOCAL
install_name_tool -change "$LIBEV_PATH" @loader_path/"$LIBEV_LOCAL" "$SERVER_BINARY"

otool -L "$SERVER_BINARY"
